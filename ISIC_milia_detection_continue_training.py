# -*- coding: utf-8 -*-
"""
Created on Thu Mar  1 21:30:18 2018
@author: akn36d

"""

import os

# Set backend to TF(Tensorflow) or Theano
USETF = True 

if USETF:
	os.environ['KERAS_BACKEND'] = 'tensorflow'
	from keras import backend as K
	K.set_image_data_format('channels_last')
else:
	os.environ['KERAS_BACKEND'] = 'theano'
	from keras import backend as K
	K.set_image_data_format('channels_first')

import sys
import glob as glob
import re
import pprint
import numpy as np
import matplotlib.pyplot as plt
import csv  
import argparse
import pandas as pd

from shutil import copyfile 

from skimage import io
from skimage.transform import resize
from sklearn.model_selection import train_test_split
from keras.callbacks import CSVLogger, EarlyStopping, ModelCheckpoint
from keras.utils import to_categorical, normalize
from keras.models import Model
from keras.optimizers import SGD, Adadelta, Adam
from Models.zf_unet_512_model2 import ZF_UNET_512_2, jacard_coef_loss, jacard_coef
# from Models.zf_unet_512_model import ZF_UNET_512, jacard_coef_loss, jacard_coef
# from Models.zf_unet_1024_model import ZF_UNET_1024, jacard_coef_loss, jacard_coef # <----------------

from utilities import *
from Models.model_utils import yuan_jaccard_loss

############################### SETTING UP ALL PARAMS ###############################
# Global Args
INPUT_CHANNELS = 3

# Model and run details of model 
model_name = 'ZF_UNET_512_2'# <------------------
run_num = 'run9'# <------------------
train_num = '3_r04'

# The config, weight and log files of previous training of model
save_config_file = os.path.join('Model_runs', 
                                model_name + '_' + run_num, 
                                'config',
                                'model_config.csv')

save_weights_path = os.path.join('Model_runs', 
                                 model_name + '_' + run_num, 
                                 'weights')
save_weights_file = os.path.join('Model_runs', 
                                 model_name + '_' + run_num, 
                                 'weights',
                                 'model_weights_3_r03.h5')
save_log_file = os.path.join('Model_runs', 
                             model_name + '_' + run_num, 
                             'logs',
                             'model.log')

print('\n The run folders are')
print(save_config_file)
print(save_log_file)
print(save_weights_file)

# Setting params of previous traiing of model 
input_height = 512
input_width = 512
learn_rate = 0.2
epochs = 200
batch_size = 4
early_stopping_patience = 4
dropout_val = 0.2
verbose_fit = 1
model_name = 'ZF_UNET_512_2'
optimizer_name = 'adam'
model_loss = 'yuan_jaccard_loss'
attr_name = 'attribute_milia_like_cyst'
seed = 90
np.random.seed(seed)

lr = learn_rate
# Optimizer 
if optimizer_name == "adam":
    optim = Adam(lr)
if optimizer_name == "sgd":
    optim = SGD(lr)
if optimizer_name == "adadelta":
    optim = Adadelta(lr)

# Loss
if model_loss == 'yuan_jaccard_loss':
  loss = yuan_jaccard_loss

# attribute
if attr_name == "attribute_milia_like_cyst":
	feature_name = 'milia'


################################# SETTING UP DATASETS #################################
# Setting up the Dataset folders
training_img_folder = os.path.join('data', 'train', 'img')
training_label_folder = os.path.join('data', 'train', 'mask')
validation_img_folder = os.path.join('data', 'val', 'img')
validation_label_folder = os.path.join('data', 'val', 'mask')

# List of images in dataset
training_label_folder_full = os.path.join(training_label_folder,
                                        feature_name + '_present',
                                        '*_' + attr_name +'.png')
tr_label_list = glob.glob(training_label_folder_full)

training_img_folder_full = os.path.join(training_img_folder,
                                      feature_name + '_present',
                                      '*.jpg')
tr_img_list = glob.glob(training_img_folder_full)

validation_label_folder_full = os.path.join(validation_label_folder,
                                          feature_name + '_present',
                                          '*_' + attr_name +'.png')
val_label_list = glob.glob(validation_label_folder_full)

validation_img_folder_full = os.path.join(validation_img_folder,
                                        feature_name + '_present',
                                        '*.jpg')
val_img_list = glob.glob(validation_img_folder_full)

# Number of images in training and validation sets    
nImgs_val = len(val_img_list)
nImgs_tr = len(tr_img_list)
print('Number of Training images : ' + str(nImgs_tr))
print('Number of Validation images : ' + str(nImgs_val))

################################## SETTING UP MODEL ##################################
# Selecting the model
model1 = ZF_UNET_512_2(dropout_val=dropout_val, weights=None) # <----------------
print(3 * '\n')
print('\nNumber of layers in network : ' + str(len(model1.layers)))

# loading the weights
model1.load_weights(save_weights_file)

# Compile model
model1.compile(loss=loss,
               optimizer=optim,
               metrics=[jacard_coef])


# Seeting up checkpoints and logger
csv_logger_call = CSVLogger(save_log_file, append=True)
model_chkpt_call = ModelCheckpoint(os.path.join(save_weights_path, 
                                                'model_weights_' + str(train_num) + '.h5'),
                                   save_best_only=True)

# Use early stopping w.r.t val_loss 
early_stopping = EarlyStopping(monitor='val_loss', 
                               patience=early_stopping_patience)

ImgGenDict = dict(seed = 42,
                  samplewise_center=True,
                  samplewise_std_normalization=True,
                  width_shift_range=0.2,
                  height_shift_range=0.2,
                  horizontal_flip=True,
                  vertical_flip=True,
                  rotation_range=45,
                  zoom_range=0.2,
                  fill_mode="constant")

ValGenDict = dict(seed = 42,
                  samplewise_center=True,
                  samplewise_std_normalization=True,
                  width_shift_range=0,
                  height_shift_range=0,
                  horizontal_flip=False,
                  vertical_flip=False,
                  rotation_range=45,
                  zoom_range=0,
                  fill_mode="constant")

TrImageGen = get_generator(training_img_folder, 
                           training_label_folder,
                           target_size=(input_height,input_width),
                           batch_size=batch_size,
                           **ImgGenDict)
ValImageGen = get_generator(validation_img_folder, 
                            validation_label_folder,
                            target_size=(input_height,input_width),
                            batch_size=batch_size,
                            **ValGenDict) 

# TRAINING THE MODEL
history = model1.fit_generator(TrImageGen,
                               steps_per_epoch=nImgs_tr//batch_size,                               
                               epochs=epochs,
                               verbose=1,
                               callbacks=[csv_logger_call,model_chkpt_call],
                               validation_data=ValImageGen,
                               validation_steps=nImgs_val//batch_size)

################################# SAVING CONFIG DICT #################################
