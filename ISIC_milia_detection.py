# -*- coding: utf-8 -*-
"""
Created on Thu Mar  1 21:30:18 2018
@author: akn36d

"""

import os

# Set backend to TF(Tensorflow) or Theano
USETF = True 

if USETF:
	os.environ['KERAS_BACKEND'] = 'tensorflow'
	from keras import backend as K
	K.set_image_data_format('channels_last')
else:
	os.environ['KERAS_BACKEND'] = 'theano'
	from keras import backend as K
	K.set_image_data_format('channels_first')

import sys
import glob as glob
import re
import pprint
import numpy as np
import matplotlib.pyplot as plt
import csv  
import argparse

from shutil import copyfile 

from skimage import io
from skimage.transform import resize
from sklearn.model_selection import train_test_split
from keras.callbacks import CSVLogger, EarlyStopping, ModelCheckpoint
from keras.callbacks import ReduceLROnPlateau, LearningRateScheduler, TensorBoard
from keras.utils import to_categorical, normalize
from keras.models import Model
from keras.optimizers import SGD, Adadelta, Adam
from Models.zf_unet_512_model2 import ZF_UNET_512_2, jacard_coef_loss, jacard_coef
# from Models.zf_unet_512_model import ZF_UNET_512, jacard_coef_loss, jacard_coef
# from Models.zf_unet_1024_model import ZF_UNET_1024, jacard_coef_loss, jacard_coef # <----------------

from utilities import *
from Models.model_utils import yuan_jaccard_loss

############################### SETTING UP ALL PARAMS ###############################
run_num = 'run12' # Each run reflects a fresh training of model with new parameters
                  # it is dfferent from train_num
train_num = 1 # Number of times this network has been trained with 
		      # previous weights but same param settings. If settings changed it
		      # should produce another config log with the train_num in its 
		      # filename. It should also append the metrics in the logs folder
		      # to the previous log file if training is continued.

# Setting the random number generator so that the results are reproducible
rand_seed = 23
np.random.seed(rand_seed)

# Global Args
INPUT_CHANNELS = 3

# Argument Parser
parser = argparse.ArgumentParser(fromfile_prefix_chars='@')
parser.add_argument("--input_height", type = int , default = None ) 
parser.add_argument("--input_width", type = int , default = None ) 
parser.add_argument("--learn_rate", type = float , default = None)
parser.add_argument("--epochs", type = int, default = None )
parser.add_argument("--batch_size", type = int, default = None)
parser.add_argument("--early_stopping_patience", type = int, default = None)
parser.add_argument("--dropout_val", type = float, default = None)
parser.add_argument("--verbose_fit", type = int , default = None)
parser.add_argument("--model_name", type = str , default = None) 
parser.add_argument("--optimizer_name", type = str , default = None)
parser.add_argument("--model_loss", type = str , default = None)
parser.add_argument("--attr_name", type = str , default = None)
# parser.add_argument("--train_img_dir", type = str, default = None)
# parser.add_argument("--train_mask_dir", type = str, default = None)
# parser.add_argument("--val_img_dir", type = str, default = None)
# parser.add_argument("--val_mask_dir", type = str, default = None)

args = parser.parse_args() # dict of parsed args

config_dict = vars(args)
config_dict['seed'] = rand_seed
config_dict['train_num'] = 1  

# Model Parameters
model_name = args.model_name
attr_name = args.attr_name
model_loss=args.model_loss

# Network and training params
lr = args.learn_rate
if args.optimizer_name == "adam":
    optim = Adam(lr)
if args.optimizer_name == "sgd":
    optim = SGD(lr)
if args.optimizer_name == "adadelta":
    optim = Adadelta(lr)
 
batch_size = args.batch_size
epochs = args.epochs
verbose_in_fit = args.verbose_fit
early_stopping_patience = args.early_stopping_patience
dropout_val = args.dropout_val
input_width, input_height = args.input_width, args.input_width

# Loss being used
if args.model_loss == "yuan_jaccard_loss":
	model_loss = yuan_jaccard_loss
if args.model_loss == "jacard_coef_loss":
	model_loss = jacard_coef_loss

# Attribute of interest
if attr_name == "milia_like_cyst":
	feature_name = 'milia'
if attr_name == "pigment_network":
	feature_name = "pigment"
if attr_name == "negative_network":
	feature_name = "neg"
if attr_name == "streaks":
	feature_name = "streak"
if attr_name == "globules":
	feature_name = "glob"

# Setting up environment paths
models_folder, log_full_path, log_dir_path, save_config_full_path, save_weights_full_path = run_environ_setup(model_name, run_num)


################################# SETTING UP DATASETS #################################
# Setting up the Dataset folders
training_img_folder = os.path.join('data', 'train', 'img')
training_label_folder = os.path.join('data', 'train', 'mask')
validation_img_folder = os.path.join('data', 'val', 'img')
validation_label_folder = os.path.join('data', 'val', 'mask')

# List of images in dataset
training_label_folder_full = os.path.join(training_label_folder,
	                                      feature_name + '_present',
	                                      '*_attribute_' + attr_name +'.png')
tr_label_list = glob.glob(training_label_folder_full)

training_img_folder_full = os.path.join(training_img_folder,
	                                    feature_name + '_present',
	                                    '*.jpg')
tr_img_list = glob.glob(training_img_folder_full)

validation_label_folder_full = os.path.join(validation_label_folder,
	                                        feature_name + '_present',
	                                        '*_attribute_' + attr_name +'.png')
val_label_list = glob.glob(validation_label_folder_full)

validation_img_folder_full = os.path.join(validation_img_folder,
	                                      feature_name + '_present',
	                                      '*.jpg')
val_img_list = glob.glob(validation_img_folder_full)

# Number of images in training and validation sets    
nImgs_val = len(val_img_list)
nImgs_tr = len(tr_img_list)
print('Number of Training images : ' + str(nImgs_tr))
print('Number of Validation images : ' + str(nImgs_val))

# Storing info in config dict
config_dict['training_img_folder'] = os.path.join(training_img_folder, feature_name + '_present')
config_dict['training_label_folder'] = os.path.join(training_label_folder, feature_name + '_present')
config_dict['validation_img_folder'] = os.path.join(validation_img_folder, feature_name + '_present')
config_dict['validation_label_folder'] = os.path.join(validation_label_folder, feature_name + '_present')
config_dict['nImgs_tr'] = nImgs_tr
config_dict['nImgs_val'] = nImgs_val


################################# SAVING CONFIG DICT #################################
# Saving configuration values in a csv file
with open(save_config_full_path,'w') as f:
  w = csv.writer(f)
  w.writerows(config_dict.items())


################################## SETTING UP MODEL ##################################
# Selecting the model
model1 = ZF_UNET_512_2(dropout_val=dropout_val, weights=None) # <----------------
print(3 * '\n')
print('\nNumber of layers in network : ' + str(len(model1.layers)))

# Print the model summary
# model1.summary()

# Compile model
model1.compile(loss=model_loss,
               optimizer=optim,
               metrics=[jacard_coef])

# Setting up checkpoints and logger
csv_logger_call = CSVLogger(log_full_path)
model_chkpt_call = ModelCheckpoint(save_weights_full_path,
                                   save_best_only=True,
                                   mode='min')
# early_stopping = EarlyStopping(monitor='val_loss', 
#                                patience=early_stopping_patience)
reduce_lr = ReduceLROnPlateau(monitor='val_loss', 
                              factor=0.5,
                              patience=4, 
                              min_lr=0.001)
# lr_scheduler = LearningRateScheduler(schedule, verbose=0)

tensor_log_path = os.path.join(log_dir_path,'tb_log')

# Setting up Tensorboard
tensorbrd = TensorBoard(log_dir=tensor_log_path, 
            histogram_freq=0, 
            batch_size=batch_size, 
            write_graph=True, 
            write_grads=False, 
            write_images=False, 
            embeddings_freq=0, 
            embeddings_layer_names=None, 
            embeddings_metadata=None)

# Setting up data generators
ImgGenDict = dict(seed = 42,
	              samplewise_center=True,
                  samplewise_std_normalization=True,
                  width_shift_range=0.5,
                  height_shift_range=0.5,
                  horizontal_flip=True,
                  vertical_flip=True,
                  rotation_range=45,
                  zoom_range=0.0,
                  fill_mode="constant")

ValGenDict = dict(seed = 42,
	              samplewise_center=True,
                  samplewise_std_normalization=True,
                  width_shift_range=0,
                  height_shift_range=0,
                  horizontal_flip=False,
                  vertical_flip=False,
                  rotation_range=0,
                  zoom_range=0,
                  fill_mode="constant")

TrImageGen = get_generator(training_img_folder, 
                           training_label_folder,
                           target_size=(input_height,input_width),
                           batch_size=batch_size,
                           **ImgGenDict)
ValImageGen = get_generator(validation_img_folder, 
                            validation_label_folder,
                            target_size=(input_height,input_width),
                            batch_size=batch_size,
                            **ValGenDict) 

# TRAINING THE MODEL
history = model1.fit_generator(TrImageGen,
                               steps_per_epoch=nImgs_tr//batch_size,                               
                               epochs=epochs,
                               verbose=1,
                               callbacks=[csv_logger_call, model_chkpt_call, tensorbrd, reduce_lr],
                               validation_data=ValImageGen,
                               validation_steps=nImgs_val//batch_size)

print(save_weights_full_path)
print(log_full_path)
