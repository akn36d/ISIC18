# -*- coding: utf-8 -*-
"""
Created on Thu Mar  1 21:30:18 2018
@author: akn36d

"""

import os

# Set backend to TF(Tensorflow) or Theano
USETF = True 

if USETF:
	os.environ['KERAS_BACKEND'] = 'tensorflow'
	from keras import backend as K
	K.set_image_data_format('channels_last')
else:
	os.environ['KERAS_BACKEND'] = 'theano'
	from keras import backend as K
	K.set_image_data_format('channels_first')

import sys
#import glob2 as glob
import glob as glob
import pprint
import numpy as np
import csv  
import argparse

from skimage import io
from skimage.transform import resize
from keras.callbacks import CSVLogger, EarlyStopping, ModelCheckpoint
from keras.models import Model
from keras.optimizers import SGD, Adadelta, Adam
from keras.preprocessing import image
from Models.zf_unet_512_model2 import ZF_UNET_512_2, jacard_coef_loss, jacard_coef
# from Models.zf_unet_512_model import ZF_UNET_512, jacard_coef_loss, jacard_coef
# from Models.zf_unet_1024_model import ZF_UNET_1024, jacard_coef_loss, jacard_coef # <----------------

from utilities import *

# Setting the random number generator so that the results are reproducible
rand_seed = 5
np.random.seed(rand_seed)
dropout_val = 0.4

# Loading up the model
model1 = ZF_UNET_512_2(dropout_val=dropout_val, weights=None) # <----------------
print(3 * '\n')
print('LOADING THE MODEL')
print('Number of layers in network : ' + str(len(model1.layers)))

# Loading the weights
model1.load_weights('./Model_runs/ZF_UNET_512_2_run9/weights/model_weights_3_r03.h5')
print('MODEL LOADED\n')

target_size = (512,512)

# Test generator
validation_img_folder = os.path.join('data',
                                     'val',
                                     'img',
                                     'milia_present')
validation_label_folder = os.path.join('data',
                                       'val',
                                       'mask',
                                       'milia_present')
# The test image folder
test_img_dir = validation_img_folder 

# test_img_dir = "./data/test/test_imgs/tr_img_milia_absent/"

# Folder to save predicted masks
save_predicted_dir = os.path.join('data',
                                  'test',
                                  'predicted_milia_run9_weights_3_r03')
makefolder_ifnotexists(save_predicted_dir) 

test_img_list = glob.glob(os.path.join(test_img_dir,'*.jpg'))

thresh = 0.2
for nIdx in range(len(test_img_list)):
	img_file_fullpath = test_img_list[nIdx]
	_, file_fullname = os.path.split(img_file_fullpath)
	img_name, _ = os.path.splitext(file_fullname)

	print('Processing img {}: {}/{}'.format(img_name,nIdx+1,len(test_img_list)))
	
	img = io.imread(test_img_list[nIdx],mode='reflect').astype(np.float32)
	img /= 255.0

	img_shape = img.shape[0:2]
	print('Max img: {}'.format(np.max(img)))
	print('Min img: {}'.format(np.min(img)))
	# print(img)

	
	# print(img_shape)
	img = (resize(img,target_size))
	# print('Max img: {}'.format(np.max(img)))
	# print('Min img: {}'.format(np.min(img)))
	# print(img)
	
	# img = standardize_image(img)
	img = img[np.newaxis,...]

	mask_predicted = model1.predict(img) 
	mask_predicted = mask_predicted[0,...,0]	
	# print('Predict shape : {}'.format(mask_predicted.shape))
	# print('Max Mask: {}'.format(np.max(mask_predicted)))
	# print('Min Mask: {}'.format(np.min(mask_predicted)))
	# print(type(mask_predicted[1,1]))
	# print(mask_predicted)
	print(2*'\n')
	# quit()
	mask_predicted = mask_predicted > thresh
	mask_predicted = np.uint8(mask_predicted)*255
	mask_predicted = np.uint8(resize(mask_predicted,img_shape,mode='reflect')*255)
	# print(mask_predicted.shape)
	
	io.imsave(os.path.join(save_predicted_dir, img_name + '.png'),mask_predicted)
	

