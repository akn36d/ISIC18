# -*- coding: utf-8 -*-
"""
Created on Sat Feb 24 15:00:19 2018
@author: akn36d
"""
import os
import sys
import warnings
import re
# import glob2 as glob
import glob as glob

import numpy as np
import matplotlib.pyplot as plt  
import random 

from skimage import io
from keras.preprocessing import image

'''-------------------------------------------------------------------------------------------
'''
def makefolder_ifnotexists(foldername):
    if not os.path.exists(foldername):
        os.makedirs(foldername)


'''-------------------------------------------------------------------------------------------
'''
'''
    Make sure the utilities.py folder is in the same folder as the training code 
    Sets up the folders as follows

    Models_runs
    |--Suite_name
       |--logs
       |--weights
       |--config
'''
def run_environ_setup(model_name, run_num):
    # get directory script resides in
    dirname = os.path.dirname(__file__)
    print(dirname)

    # Suite Name and directory declaration (The place where all the info for a given run 
    # of a model will be stored)
    models_folder = 'Model_runs'
    makefolder_ifnotexists(os.path.join(dirname, 
                                        models_folder))

    # Specify Model name to save model run information in Model_runs folder
    suite_dirname = model_name + '_' + run_num
    # suite_dirname = input('\nEnter name for this run of model: ')
    makefolder_ifnotexists(os.path.join(dirname, 
                                        models_folder, 
                                        suite_dirname))

    # log folder for run of model
    log_path = 'logs'
    makefolder_ifnotexists(os.path.join(dirname,
                                        models_folder,
                                        suite_dirname, 
                                        log_path))
    log_dir_path = os.path.join(dirname,
                                models_folder,
                                suite_dirname, 
                                log_path)

    # model folder for saving weights in a run of model
    save_weights_to_path = 'weights'
    makefolder_ifnotexists(os.path.join(dirname,
                                        models_folder,
                                        suite_dirname ,
                                        save_weights_to_path))

    # settings/config folder for a run of model
    config_path = 'config'
    makefolder_ifnotexists(os.path.join(dirname,
                                        models_folder,
                                        suite_dirname ,
                                        config_path))
    # Save the model accuracy and loss
    log_name = 'model.csv'
    log_full_path = os.path.join(dirname,
                                 models_folder,
                                 suite_dirname, 
                                 log_path, 
                                 log_name)

    # Save model for testing purposes
    save_weights_name =   'model_weights.h5'
    save_weights_full_path = os.path.join(dirname,
                                          models_folder,
                                          suite_dirname,
                                          save_weights_to_path,
                                          save_weights_name)

    # Save run Config
    save_config_name = 'model_config.csv'
    save_config_full_path = os.path.join(dirname,
                                         models_folder,
                                         suite_dirname ,
                                         config_path,
                                         save_config_name)
    return models_folder, log_full_path, log_dir_path, save_config_full_path, save_weights_full_path


'''-------------------------------------------------------------------------------------------
'''
def deprocess_image(x):
    # normalize tensor: center on 0., ensure std is 0.1
    x -= x.mean()
    x /= (x.std() + 1e-5)
    x *= 0.1

    # clip to [0, 1]
    x += 0.5
    x = np.clip(x, 0, 1)
    

    # convert to RGB array
    x *= 255
    x = np.clip(x, 0, 255).astype('uint8')
    return x

def standardize_image(x):
    # normalize tensor: center on 0., ensure std is 0.1
    x -= x.mean()
    x /= (np.std(x, keepdims=True) + 1e-5)
 
    return x

'''-------------------------------------------------------------------------------------------
'''
def normalize_image(x, norm_range):
    # normalize image
    print('predict_min  : {}'.format(x.min()))
    x -= x.min()
    print('predict_max  : {}'.format(x.max()))
    x *= norm_range/(x.max() + 1)
    x = x.astype(np.uint8)
    return x

'''-------------------------------------------------------------------------------------------
'''
#https://spark-in.me/post/unet-adventures-part-one-getting-acquainted-with-unet
def get_generator(train_folder, train_mask_folder,
                  target_size = (224,224),
                  batch_size = 32,
                  seed = 42,
                  samplewise_center=True,
                  samplewise_std_normalization=True,
                  width_shift_range=0.2,
                  height_shift_range=0.2,
                  horizontal_flip=True,
                  vertical_flip=True,
                  rotation_range=10,
                  zoom_range=0.2,
                  fill_mode="constant"):
    
    
    # Example taken from https://keras.io/preprocessing/image/
    # We create two instances with the same arguments
    data_gen_args = dict(
                        width_shift_range=width_shift_range,
                        samplewise_center=True,
                        samplewise_std_normalization=True,
                        height_shift_range=height_shift_range,
                        horizontal_flip=horizontal_flip,
                        vertical_flip=vertical_flip,
                        rotation_range=rotation_range,
                        zoom_range=zoom_range,
                        fill_mode=fill_mode,
                        rescale=1.0/255,
                        cval=0       
                        )

    mask_gen_args = dict(
                    width_shift_range=width_shift_range,
                    samplewise_center=False,
                    samplewise_std_normalization=False,
                    height_shift_range=height_shift_range,
                    horizontal_flip=horizontal_flip,
                    vertical_flip=vertical_flip,
                    rotation_range=rotation_range,
                    zoom_range=zoom_range,
                    fill_mode=fill_mode,
                    rescale=1.0/255,
                    cval=0       
                    )

    image_datagen = image.ImageDataGenerator(**data_gen_args)
    mask_datagen = image.ImageDataGenerator(**mask_gen_args)
    image_generator = image_datagen.flow_from_directory(
        train_folder,
        batch_size = batch_size,
        target_size = target_size,
        class_mode=None,
        color_mode='rgb',
        seed=seed)
    mask_generator = mask_datagen.flow_from_directory(
        train_mask_folder,
        batch_size = batch_size,    
        target_size = target_size,    
        class_mode=None,
        color_mode='grayscale',
        seed=seed)

    # combine generators into one which yields image and masks
    train_generator = zip(image_generator, mask_generator)

    return train_generator

'''-------------------------------------------------------------------------------------------
'''
